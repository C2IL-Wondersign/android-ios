 $(document).ready(function(){

    /*click function to view the products of particular categories*/
          $(".page").on('click', '.productlink', function(event) 
        {
          /*id value of the clicked element is the categoryid*/
          var categoryid=$(this).attr('id');
          /*var categoryid=$(this).prev().attr('id');*/
          /*alert("clicked cat id"+categoryid);*/
            /*check if there is  any product in the clicked category*/
            db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});

        db.transaction(function (tx) {
       
                       
                       tx.executeSql('SELECT count(c.product_id) as total FROM product_vs_categories c  inner join products p on c.product_id=p.product_id WHERE category_id=?', [categoryid], function (tx, results) {
                                     var length= results.rows.length;
                                     var total=results.rows.item(0).total;
                                   /* alert("total pro.."+total+"..len  "+length);  */
                                     
                                     /*alert(length);*/
                                     if(total>0)
                                     {
                                     localStorage.setItem('parentcatid', categoryid);
                                     /*localStorage.setItem('parentname', parentname);*/
                                     /*localStorage.removeItem('parentcatid');
                                      localStorage.removeItem('parentname');*/
                                     
                                     localStorage.setItem('category_id', categoryid);
                                     $(location).attr('href', 'products.html');
                                     }
                                    
                                     
                                     }, null);
                       
                       /*tx.executeSql('SELECT product_id,category_id FROM product_vs_categories WHERE category_id=?', [categoryid], function (tx, results) {
            var length= results.rows.length;
           
            if(length>0)
            {

              localStorage.setItem('category_id', categoryid);
              $(location).attr('href', 'products.html');
            }

            }, null);  */
         }); /*transaction ends*/
          
        });
});/*ready ends*/

$(".page").on('click', 'a.hasSub', function(event)
              {
              
              
              var parentid=$(this).closest("li").attr("id");
              var parentclass=$(this).closest("li").attr("class");
              /*alert("clicked"+parentid+"class.."+parentclass);*/
              /*var isVisible = $( '.maincatul #'+par ).is( ".opened" );*/
              if(parentclass=="maincatul")
              {
              var isopened=$(this).parent().find('ul').is( ".opened" );
              /*alert("is opened.."+isopened);*/
              if(isopened==false)
              {
              $('.categoryList ul ul').slideUp();
              
              }
              else if(isopened==true)
              {
              $('.categoryList > ul > li > a').removeClass('active');
              }
              }
              /*$('.categoryList ul ul').not("a.hasSub li#"+parentid+" ul li ul")slideUp();*/
              
              $(this).addClass('active').parent().find('ul').not("li#"+parentid+" ul li ul").slideToggle().toggleClass('opened');
              });


  /*generating category tree in side bar*/
   function getcategories() {
        //var k1,k2;
        /*alert("side toggle called..");*/
        /*alert(" copy success ");*/
        db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});
        /*alert("after open db.."+db);*/
        db.transaction(function (tx) {
        tx.executeSql('SELECT category_id,name FROM CATEGORY WHERE parent_category_id=0', [], function (tx, results) {
        var len = results.rows.length;
        /*To show the categories into two columns*/
            var catcolumn=len/2;
            /*alert("items in one column..."+catcolumn);*/
        /*alert("length of main CATEGORY"+len); */     
        /*alert("query executed...");*/
        for (var i = 0; i < results.rows.length; i++) 
        {                
            var categoryid=results.rows.item(i).category_id;
            subcat(categoryid);
            var catname=results.rows.item(i).name;            

            /*alert(catname);*/
             $("#categoriestree").append("<li style='font-size: 35px;' id='"+categoryid+"' class='maincatul'><a class='hasSub'><span class='productlink' style='color:#f68428;font-weight:bold;font-size: 35px !important;' id='"+categoryid+"'>"+catname+"</span></a></li>").trigger("create");  
            /*  $("#categoriestree").append("<li style='font-size: 35px;' id='"+categoryid+"' class='maincatul'><a>"+catname+"</a></li>"); */
          
        }       
  
         }, null);
      });

    }//getcategories() ends

    function subcat(parentcatid)
    {
      /*alert("subcat called");*/


        db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});

        db.transaction(function (tx) {

        /*tx.executeSql('SELECT category_id,name FROM CATEGORY WHERE parent_category_id=?', [parentcatid], function (tx, results1) {*/

          /*tx.executeSql('select c.category_id,c.name,count(p.product_id) as totalproducts from category c  left join product_vs_categories p on c.category_id = p.category_id where c.parent_category_id=? group by c.category_id', [parentcatid], function (tx, results1) {*/

            tx.executeSql('select c.category_id,c.name,count(products.product_id) as totalproducts,count(sub.category_id) as totalsubcats from category c  left join product_vs_categories p on c.category_id = p.category_id left join products on p.product_id=products.product_id left  join  category sub on sub.parent_category_id=c.category_id where c.parent_category_id=?  group by c.category_id', [parentcatid], function (tx, results1) {

          
              //alert("subcat hi2");
             var len1=results1.rows.length;
             /*alert(len1);*/
             
             /*if(len1>0)
             {
              $("#"+parentcatid).append("<ul class='"+parentcatid+"'></ul>");
             }  
             */
             $("#"+parentcatid).append("<ul class='"+parentcatid+"'></ul>").trigger("create");
             

            for(var j=0;j<len1;j++){
                var subcatname=results1.rows.item(j).name;
                var subcatid=results1.rows.item(j).category_id;
                var totalproducts=results1.rows.item(j).totalproducts;
                var totalsubcats=results1.rows.item(j).totalsubcats;
                /*alert("sub catid.."+subcatid+" .name.."+subcatname);*/
                /*alert("sub row.."+j+" "+k2);*/
                var idval="#"+parentcatid;
                if(totalsubcats>0)
                {
                  $(idval+" ul."+parentcatid).append("<li id='"+subcatid+"'><a class='hasSub'><span class='productlink' style='font-size: 35px;color:#f68428;' id='"+subcatid+"'>"+subcatname+"</span></a></li>").trigger("create"); 
                }
                else
                {
                   $(idval+" ul."+parentcatid).append("<li id='"+subcatid+"'><a><span class='productlink' style='font-size: 35px;' id='"+subcatid+"'>"+subcatname+"("+totalproducts+")"+"</span></a></li>").trigger("create"); 
                }
                /*alert(idval+" ul."+parentcatid);*/

               /* $("#categoriestree").append("<li id='"+subcatid+"'><span class='productlink' id='"+subcatid+"'>"+subcatname+"</span></li>"); */

        /*alert("sub cat appended");*/
                /*$("li #"+subcatid).hide();*/
              /*  alert("after hide...");*/

                subcat(subcatid);
                /*alert("sub cat called again..");*/

            }/*sub cat loop ends*/

            }, null);
         });
}/*sub cat function ends*/

/*generating category tree in side bar*/