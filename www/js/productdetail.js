$(document).ready(function(){
  function showOverlay() {
    $('.modalOverlay').fadeIn();
  }
  function hideOverlay() {
    $('.modalOverlay').fadeOut();
    
    $("#emailText").val("");
    $("#smsText").val("");
    
  }
  $(document).ready(function(){



    /*$("#emailtextbox").hide();*/
    $('#sendToMail').on('tap', function () {
      showOverlay();
      $('.modal').removeClass('active');
      $('#emailDialog').addClass('active');
          //$('#emailText').focus();
        });


    $('#sendTextMessage').on('tap', function () {
      showOverlay();
      $('.modal').removeClass('active');
      $('#smsDialog').addClass('active');
          //$('#smsText').focus();
        });
    $('.modalOverlay, .modalClose').on('tap', function () {
      hideOverlay();
      $('.modal').removeClass('active');
      
    });


    document.addEventListener("deviceready", onDeviceReady, false);
    var db;


    //Examples of how to assign the Colorbox event to elements
    $(".inline").colorbox({inline:true, width:"50%"});
    
    //Example of preserving a JavaScript event for inline calls.
    $("#click").click(function(){ 
      $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
      return false;
    });

    
    
    
    $(".page").on('click', '#favourites', function(event)
    {
      $(location).attr('href', 'favourites.html');
    });
    
    
    
    $(".page").on('click', '#removefav', function(event)
    {
      var product_id=localStorage.getItem('product_id');
      
      
      db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});
      
      db.transaction(function (tx) {
       var favdelete="delete from favourite_products where product_id="+product_id;
       console.log("fav delete .."+favdelete);
       console.log(favdelete);
       tx.executeSql(favdelete , [], function (tx, results) {
         
         $(".favpro").attr("src","images/star2B.png");
         $(".favpro").attr("id","addtofav");
         console.log(product_id+" ..value deleted ");
       });
     });
      
    });
    
    
    $(".page").on('click', '#addtofav', function(event)
      
    {
      /*alert("fav icon clicked");*/
      
      var product_id=localStorage.getItem('product_id');
      
      /*alert(product_id);*/
      
      
      
      
      db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});
      
      db.transaction(function (tx) {
       var favinsert="insert into favourite_products(product_id,dealer_id,user_id) values("+product_id+","+1+",1) ";
       console.log("fav insert .."+favinsert);
       console.log(favinsert);
       tx.executeSql(favinsert , [], function (tx, results) {
         $(".favpro").attr("src","images/star2A.png");
         $(".favpro").attr("id","removefav");
         console.log(product_id+" ..value inserted ");
       });
     });
      
      
      
      
      
      
      
      
    });
    

    $("#additionalImages").on('click', '.additionalImage', function(event) 
    {
      var productid=$(this).attr('id');
      /*alert(productid);*/
      localStorage.setItem('product_id', productid);
      $(location).attr('href', 'ormond-beach-firm.html');

    });

    $(".send").click(function()
    {

      var id = $(this).attr("id");
      var device_id = localStorage.getItem("device_id")
      var emailId = $("#emailText").val();
      var mobile_number = $("#smsText").val();
      /*alert(device_id);*/
      if(id=="emailButton")
      {
        /*       email validation start  +++++++++++++++*/
        
        if (emailId == null || emailId == "") {
          
          navigator.notification.alert(
            "Please enter the Email Address ",    // message
            null,       // callback
            "Alert", // title
            'OK'        // buttonName
            );
          return false;
        }
        var atpos = emailId.indexOf("@");
        var dotpos = emailId.lastIndexOf(".");

        if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=emailId.length) {
         $("#emailText").val("");
         navigator.notification.alert(
            "Not a valid e-mail address",    // message
            null,       // callback
            "Alert", // title
            'OK'        // buttonName
            );
         
         return false;
       }

       /*       email validation Ends  +++++++++++++++*/

       var emailSend = "api_key=OXe5fWizFQhu4acLd37llxqaYoEcryS1&device_id="+device_id+"&product_id="+localStorage.getItem("product_id")+"&email_address="+emailId;


       console.log(emailSend)
       $.ajax({
        type: "POST", 
        url: "http://ashley.catalogkiosk.com/api/v1/send_email",
        contentType: "application/x-www-form-urlencoded",
        data :emailSend,
        dataType: "json",                   
        async: true, 

        success:function(result)
        {
         console.log(JSON.stringify(result));
         $(".modalClose").trigger("click");

         navigator.notification.alert(
            result.message,    // message
            null,       // callback
            "Alert", // title
            'OK'        // buttonName
            );
         
         $("#emailText").val("");
         hideOverlay();
         $('.modal').removeClass('active');
         
         
       },
       error:function(err)
       {
         $(".modalClose").trigger("click");
         $("#emailText").val("");
         hideOverlay();
         $('.modal').removeClass('active');
         console.log(JSON.stringify(err));

         navigator.notification.alert(
            "Unable to send email",    // message
            null,       // callback
            "Alert", // title
            'OK'        // buttonName
            );


       }
     });

}
else if(id=="smsButton")
{
  
  /*       Mobile validation start  +++++++++++++++*/
  if (mobile_number == null || mobile_number == "" || mobile_number.length >11 || isNaN(mobile_number)==true)
  {
   $("#smsText").val("");
   
   navigator.notification.alert(
            "Please enter a valid Mobile Number",    // message
            null,       // callback
            "Alert", // title
            'OK'        // buttonNamer
            );
   return false;
 }
 

 /*       Mobile validation Ends  +++++++++++++++*/
 var smsSend = "api_key=OXe5fWizFQhu4acLd37llxqaYoEcryS1&device_id="+device_id+"&product_id="+localStorage.getItem("product_id")+"&mobile_no="+mobile_number;
 console.log(smsSend)
 $.ajax({

  type: "POST", 
  url: "http://ashley.catalogkiosk.com/api/v1/send_sms",
  contentType: "application/x-www-form-urlencoded",
  data :smsSend,
  dataType: "json",                   
  async: true, 

  success:function(result)
  {
   console.log(JSON.stringify(result));
   
   $("#smsText").val("");
   
   hideOverlay();
   $('.modal').removeClass('active');
   $(".modalClose").trigger("click");

   navigator.notification.alert(
            result.message,    // message
            null,       // callback
            "Alert", // title
            'OK'        // buttonName
            );
   
   
   
   
 },
 error:function(result)
 {
   $(".modalClose").trigger("click");
   $("#smsText").val("");
   
   hideOverlay();
   $('.modal').removeClass('active');
   
   console.log(JSON.stringify(result));

   navigator.notification.alert(
            "Unable to send message",    // message
            null,       // callback
            "Alert", // title
            'OK'        // buttonName
            );

   
 }

});
}


});




});
/*ready function ends*/

function onDeviceReady() {
  /*alert("device ready");*/
  heartBeat(); 

  /*alert("before copy");*/
  window.plugins.sqlDB.copy("ashley.sqlite", copySuccess, copyError);
  /*alert("after copy");  */
  document.addEventListener("backbutton", onBackKeyDown, false);
  appendLogo();
}
function onBackKeyDown() {   
 
 $(location).attr('href',"products.html"); 
}

function copySuccess() {
  
 
  db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});
  /*alert("db object.."+db);*/

  /*get product_id from local storage*/
  var product_id=localStorage.getItem('product_id');
  /*alert(product_id);*/

  db.transaction(function (tx) {
   /*tx.executeSql("SELECT image_url FROM PRODUCT_VS_IMAGES WHERE product_id=219" , [], function (tx, results) {*/

     var checkmarktng="select in_marketing from dealer_device_info where id=1"
     tx.executeSql(checkmarktng , [], function (tx, results) {
       var length=results.rows.length;
       var in_marketing=results.rows.item(0).in_marketing;
       console.log("in..marketing.."+in_marketing);
       if(in_marketing==1)
       {
         
         var favgry="select * from favourite_products where product_id="+product_id;
         tx.executeSql(favgry , [], function (tx, results) {
           
           var length=results.rows.length;
           if(length>0)
           {
             $("#favimage").append('<img class="favpro" src="images/star2A.png" id="removefav" style="height:60px;width:60px">');
             
             
           }
           else
           {
             $("#favimage").append('<img src="images/star2B.png" class="favpro" id="addtofav" style="height:60px;width:60px">');	
           }
         });
       }
     });
     
     /*get product details using the product id*/
     var prodetQuery="select name as itemname,description as itemdesc,weight,sku,price_wholesale,image_url,local_image_name from products LEFT JOIN product_vs_images ON products.product_id=product_vs_images .product_id where products.product_id="+product_id+" group by products.product_id";
     /*alert(prodetQuery);*/
     tx.executeSql(prodetQuery , [], function (tx, results) {
      
      /*alert("results..."+JSON.stringify(results));*/
      var len = results.rows.length;
      var itemname=results.rows.item(0).itemname;
      var itemdescription=results.rows.item(0).itemdesc;
      var model=results.rows.item(0).sku;
      var weight=results.rows.item(0).weight;
      var imageurl=results.rows.item(0).image_url;
      var local_image_name=results.rows.item(0).local_image_name;
      var price=results.rows.item(0).price_wholesale;
      localStorage.setItem("img_name",local_image_name);

      var searchname=itemname.substring(0,4);
      
      $("#seriesname").append("<td>"+itemname+"</td>").trigger("create");
      $("#itemname").append("<td>"+itemname+"</td>").trigger("create");
      $("#model").append("<td>"+model+"</td>").trigger("create");
      $("#weight").append("<td>"+weight+"</td>").trigger("create");  

      $("#descriptionText").append(itemdescription).trigger("create");
      var pricequery="select display_price from dealer_device_info where id=1"
      tx.executeSql(pricequery , [], function (tx, results) {
        
        var length=results.rows.length;
        var display_price=results.rows.item(0).display_price;
        /*alert("display_price "+display_price);*/
        console.log("display_price length.."+length +"  data.."+JSON.stringify(results));
        if(display_price==1 && price>0)
        {
          $("#pricetag").text("Price: $"+price);
          $("#tbody").append('<tr id="weight"><td >Price</td><td class="td-right">'+price+'</td></tr>');
          
        }
        
        
        
      });

      
      var relproQuery="SELECT cat.product_id,cat.category_id,img.image_url,pro.name,img.local_image_name FROM product_vs_categories cat LEFT JOIN product_vs_images img ON cat.product_id= img.product_id inner join products pro on cat.product_id=pro.product_id  where cat.category_id =(SELECT category_id FROM product_vs_categories WHERE product_id="+product_id+") AND pro.name like '"+searchname+"%' AND cat.product_id NOT IN ("+product_id+") GROUP BY cat.product_id order by price_wholesale desc";
      
      tx.executeSql(relproQuery , [], function (tx, results) {
        /*alert("inside rel pro");*/
        var length=results.rows.length;
        
        
        for (i = 0; i < 4; i++){
          
          var img_name  =  JSON.stringify(results.rows.item(i).local_image_name);
          /*  alert("image  name.."+img_name);  */
          var product_id = JSON.stringify(results.rows.item(i).product_id);
          var local_image_name = img_name.substring(1, img_name.length-1);
          var nativePath = localStorage.getItem("nativePath");
      // var filePath = nativePath+"Ashley/"+local_image_name;
      var filePath = "cdvfile://localhost/persistent/Ashley/"+local_image_name;
      var image_url = JSON.stringify(results.rows.item(i).image_url);

      var location = filePath;

      window.resolveLocalFileSystemURL(location,function(oFile){
        oFile.file(function(readyFile){
          var reader = new FileReader();

          reader.onloadend = function(evt){
            $("#additionalImages").append('<div class="additionalImage" id="'+product_id+'"><img src="'+evt.target.result+'" border="0" /></div>').trigger("create");
            // $("#detailMainImage").append('<img src="'+evt.target.result+'" border="0" alt="product image"/>').trigger("create");
                               };//function(evt)
                               reader.readAsDataURL(readyFile);

                             });
      },function(err)
      {
        console.log("Error")
        $("#additionalImages").append('<div class="additionalImage" id="'+productid+'"><img src="'+evt.target.result+'" border="0" /></div>').trigger("create");

      }
)//resolved filesys url
    }

  }, null);



var filePath = "cdvfile://localhost/persistent/Ashley/"+local_image_name;


var location = filePath;

window.resolveLocalFileSystemURL(location,function(oFile){
  oFile.file(function(readyFile){
    var reader = new FileReader();

    reader.onloadend = function(evt){
      $("#detailMainImage").append('<img src="'+evt.target.result+'" border="0" alt="product image"/>').trigger("create");
                               // $("#detailMainImage").smartZoom();
                               
                               };//function(evt)
                               reader.readAsDataURL(readyFile);

                             });
},function(err)
{
  console.log("Error")
  $("#detailMainImage").append('<img src="'+imageurl+'" border="0" alt="product image"/>').trigger("create");
    // $("#detailMainImage").smartZoom();

  }

  )
}, null);


$("#detailMainImage").on("click","img",function(){
  
  var src = $(this).attr("src");
  localStorage.setItem("image_zoom",src);
  $(location).attr("href","image-zoom.html");
})


});
/*calling category side toggle side */
getcategories();
/*alert("after get cats");*/

} /*copy success ends*/

function copyError(e) 
{   
  copySuccess();
  /*alert(e);*/ 
}

})



