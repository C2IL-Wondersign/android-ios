  $(document).ready(function(){
    
   
    
    var interval;

    var overlay = jQuery('<div id="overlay"> </div>');
    overlay.appendTo(document.body);


    $("#back").on("click",function(){
      $(location).attr("href","roomcategories.html");
      
      
      
    })


    $("#productGridSlider").hide();


    document.addEventListener("deviceready", onDeviceReady, false);
    var db;

    function onDeviceReady() {
// Copying SQLite DB from assets to device
heartBeat(); 

window.plugins.sqlDB.copy("ashley.sqlite", copySuccess, copyError);
document.addEventListener("backbutton", onBackKeyDown, false);


appendLogo();


}
function onBackKeyDown() {   
  
  $(location).attr('href',"roomcategories.html"); 
}

function copySuccess() {
    // open db and run your queries

    db = window.sqlitePlugin.openDatabase({name:"ashley.sqlite"});

// Creating view by joining product, product_vs_categories, product_vs_images
db.transaction(function(tx){
  tx.executeSql("create view product_slider1 as select * from product_vs_categories as one left join product_vs_images as two on one.product_id = two.product_id inner join products as three on two.product_id = three.product_id");

  copyError()

})
} 
// called when db exists
function copyError() {
  
  /*method to generate side catelog tree*/
                    // getcategories();
    // db already exists or problem in copying the db file. Check the Log.
// getting category id
var category_id = localStorage.getItem("category_id");
console.log("category_id: "+category_id)
db = window.sqlitePlugin.openDatabase({name:"ashley.sqlite"});
    // db = window.openDatabase("ashley.sqlite","1.0","Ashley DB",200000)
    db.transaction(function(tx){
      tx.executeSql("create view product_slider1 as select * from product_vs_categories as one inner join product_vs_images as two on one.product_id = two.product_id inner join products as three on two.product_id = three.product_id");
      // getting products based on category id
      tx.executeSql("Select * from product_slider1 where category_id="+category_id+" group by product_id",[],function(tx,res){

        var box = 0
        var id_num =0;
        var loop_length = res.rows.length;

        // for(var i = 0; i<res.rows.length;i++)
        var i = 0;

        interval = setInterval(function() {
          console.log("loop: "+i)

          var img_name  =  JSON.stringify(res.rows.item(i).local_image_name);

          var local_image_name = img_name.substring(1, img_name.length-1);

          var nativePath = localStorage.getItem("nativePath");

// var filePath = nativePath+"Ashley/"+local_image_name;
var filePath = "cdvfile://localhost/persistent/Ashley/"+local_image_name;
var product_id = JSON.stringify(res.rows.item(i).product_id);
var name = JSON.stringify(res.rows.item(i).name);
var imageurl = JSON.stringify(res.rows.item(i).image_url);
i=i+1;
appendImage(filePath,product_id,name,loop_length,imageurl)

},100)

      })

    })



}

function appendBxSlider()  
{
  var slideno = localStorage.getItem("slide_number");
  var slide;
  var slength = $(".productGridSlide").length;
// activating slider
$('#productGridSlider').trigger("create")
if(slength>1)
{
  if(slideno!=null)
  {
    slide = $('#productGridSlider').bxSlider({
      startSlide:slideno,
      infiniteLoop: false,
      adaptiveHeight:true,
      hideControlOnEnd: true,
      autoHidePager:true

    });
    $("#loading").remove();
    $("#overlay").remove();
    $("#sync").remove();
    $("#productGridSlider").show(); 

  }
  else
  {
   slide = $('#productGridSlider').bxSlider({
    infiniteLoop: false,
    adaptiveHeight:true,
    hideControlOnEnd: true,
    autoHidePager:true
    
  });
   $("#loading").remove();
   $("#overlay").remove();
   $("#sync").remove();
   $("#productGridSlider").show();
 }

}
else{
  $("#loading").remove();
  $("#overlay").remove();
  $("#sync").remove();
  $("#productGridSlider").show();

} 


// getting product id and sending the product id to product detail page
$("#productGridSlider").on("click","img",function(){
  if(slength>1)
  {
    var slide_number = slide.getCurrentSlide();

    localStorage.setItem("slide_number",slide_number);
  }
  var id_img = $(this).attr('id');
  console.log("productID: "+id_img)
  localStorage.setItem("product_id",id_img);
  $(location).attr("href","ormond-beach-firm.html");


})

}
var loop_count = 0;
var count = 0;
function appendImage(filePath,product_id,name,loop_length,imageurl)
{

  var location = filePath;

  console.log("append: "+count)
  window.resolveLocalFileSystemURL(location,function(oFile){
    oFile.file(function(readyFile){
      var reader = new FileReader();

      reader.onloadend = function(evt){
              // $("#largeImage").css({"display":"block"});      
              
              if(count > 5 || count == 0)
              {
                $("#productGridSlider").append("<div  class='productGridSlide '></div>")
                count = 0;
              }
              count = count +1;
              console.log("success: "+count)
              
              $("#productGridSlider div.productGridSlide:last").append("<div class= 'box box"+count+"'><img style='display:block;' id=" +product_id +" src="+evt.target.result+ " class='productGridImage'><div class='productGridSeriesName'>"+name+"</div></div>")

              loop_count = loop_count+1;

              console.log("loop length-->"+loop_length+" - "+ "loop_count-->"+loop_count);
              if(loop_count==loop_length )
              {
                clearInterval(interval);

                console.log("calling bx slider")
                appendBxSlider();
              }


            };
            reader.readAsDataURL(readyFile); 

          });
},function(err)
{
  // append image from url when the image is not present in local memory
  if(err.code==1)
  {
    $("#productGridSlider").hide();

    if(count > 5 || count == 0)
    {
      $("#productGridSlider").append("<div  class='productGridSlide '></div>")
      count = 0;
    }
    count = count +1;
    console.log("success: "+count)
    
    $("#productGridSlider div.productGridSlide:last").append("<div class= 'box box"+count+"'><img style='display:block;' id=" +product_id +" src="+imageurl+ " class='productGridImage'><div class='productGridSeriesName'>"+name+"</div></div>")

    loop_count = loop_count+1;

    console.log("loop length-->"+loop_length+" - "+ "loop_count-->"+loop_count);
    if(loop_count==loop_length )
    {
      clearInterval(interval);

      console.log("calling bx slider")
      appendBxSlider();
    }

  }           
  
  else
  {
   
   navigator.notification.alert(
              "Connection Error",    // message
              null,       // callback
              "Alert", // title
              'OK'        // buttonName
              );
 }
 
}
)
  console.log("point:"+ count)

}


})


