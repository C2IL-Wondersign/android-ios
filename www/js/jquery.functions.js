/**
 * User: tf
 * Date: 23.09.2014
 * Time: 13:37
 */
 var ratio = 0;
 var idleTime = 0;
 var widthHeightRatio = 0;

 function getWidthHeightRatio(){
  widthHeightRatio = $(window).width() / $(window).height();
  if(widthHeightRatio <= 1.35){
    $('body').addClass('res4to3');
  } else {
    $('body').removeClass('res4to3');
  }
}

function scaleApp() {
  var ww = $(window).width();
  var mw = 1366;
     // min width of site
     ratio = ww / mw;
     $('#wrapper')
     .css({
       '-webkit-transform': 'scale(' + ratio + ')',
       '-moz-transform': 'scale(' + ratio + ')',
       '-ms-transform': 'scale(' + ratio + ')',
       '-o-transform': 'scale(' + ratio + ')',
       'transform': 'scale(' + ratio + ')'
     });
   }

   function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 3) { // 15 sec
      $('#touchOverlay, #touchPulse, #touchPointer').fadeIn();
    }
  }

  function setIdleToZero() {
    idleTime = 0;
    $('#touchOverlay, #touchPulse, #touchPointer').hide();
  }

  function showGridView(){
    hideCatalog();
    hideSearch();
    /* $('.homebutton').hide();*/
    $('.view').removeClass('active');
    $('.gridView').addClass('active');
  }

  function showDetailView(){
    hideCatalog();
    hideSearch();
    $('.homebutton').show();
    $('.view').removeClass('active');
    $('.detailView').addClass('active');

  }

  function showSearchView(){
    hideCatalog();
    hideSearch();
    $('.homebutton').show();
    $('.view').removeClass('active');
    $('.searchView').addClass('active');
  }

  function toggleCatalog(){
    hideSearch();
    $('#catalog, .catalog').toggleClass('active');
  }

  function hideCatalog(){
    $('#catalog, .catalog').removeClass('active');
  }

  function toggleSearch(){
    hideCatalog();
    $('#searchField, .searchButton').toggleClass('active');
    $('#searchField.active #searchInput').focus();
  }

  function hideSearch(){
    $('#searchField, .searchButton').removeClass('active');
    $('#searchInput').blur();
  }

  function setContentDimensions() {
    $('#catalog').height($('#wrapper').height() - $('.pageHeader').height()-parseInt($('#catalog').css('padding-top')) -parseInt($('#catalog').css('padding-bottom')) );
    $('#catalog, #searchField').css('top',$('.pageHeader').height());
    $('.pageContent').height($('#wrapper').height() - $('.pageHeader').height() - parseInt($('.pageContent').css('padding-top')) - parseInt($('.pageContent').css('padding-bottom')) - parseInt($('.pageContent').css('margin-top')) - parseInt($('.pageContent').css('margin-bottom')));
  }

  function downloadLogo(image_url,local_image_name) {

    currentTime("Logo download starts");
          //    navigator.notification.alert(
          //     "Image Download Completed",    // message
          //     null,       // callback
          //     "Alert", // title
          //     'OK'        // buttonName
          // );



var check = null;
var fileTransfer = new FileTransfer();
var url = image_url;
  // var filePath = nativePath+window.appRootDirName+"/"+local_image_name;
  var filePath = "cdvfile://localhost/persistent/Ashley/"+local_image_name;
  console.log("filePath--> "+filePath)
  console.log("url--> "+url)
  fileTransfer.download(
    url, filePath, function(entry) {
      check = entry.fullPath;
      console.log("Logo download complete: " + check);

    }, function(error) {
      console.log(JSON.stringify(error))
      console.log("download error" + error.source);
      console.log("download error target " + error.target);
      console.log("upload error code" + error.code);
    });

}



function appendLogo(){

// dealer logo
var filePath = "cdvfile://localhost/persistent/Ashley/logo.jpg";
var location = filePath;

window.resolveLocalFileSystemURL(location,function(oFile){
  oFile.file(function(readyFile){
    var reader = new FileReader();

    reader.onloadend =   function(evt){
      $(".logo").attr("src",evt.target.result);
            // $(".logo").attr('<img src="'+evt.target.result+'" border="0" alt="product image"/>').trigger("create");
                               };//function(evt)
                               reader.readAsDataURL(readyFile);

                             });
},function(err)
{
  console.log("Error")
  $(".logo").attr("src",localStorage.getItem("logo"));

}

)

}
function currentTime(activity)
{
  var dNow = new Date();
  var localdate= (dNow.getMonth()+1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
  console.log(activity+"-->"+localdate);

}
function heartBeat1()
{

  console.log("heartbeat")
// heartbeat starts
var heart = "api_key=OXe5fWizFQhu4acLd37llxqaYoEcryS1&device_id="+localStorage.getItem("device_id");
console.log(heart);

$.ajax({
  type: "POST", 
  url: "http://ashley.catalogkiosk.com/api/v1/send_heartbeat",
  contentType: "application/x-www-form-urlencoded",
  data :heart,
  dataType: "json",                   
  async: true, 

  success:function(result)
  {
    var dNow = new Date();
    var localdate= (dNow.getMonth()+1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
    console.log(result.message+" Device is active @ "+localdate)
    deviceInfo()
  },
  error:function(err)
  {

   console.log("Device is inactive")


 }
})

// heart beat ends

}

function heartBeat()
{

  console.log("heartbeat")
// heartbeat starts
heartBeat1();
var heart = "api_key=OXe5fWizFQhu4acLd37llxqaYoEcryS1&device_id="+localStorage.getItem("device_id");
console.log(heart);
var heartBeat = setInterval(function(){

  $.ajax({
    type: "POST", 
    url: "http://ashley.catalogkiosk.com/api/v1/send_heartbeat",
    contentType: "application/x-www-form-urlencoded",
    data :heart,
    dataType: "json",                   
    async: true, 

    success:function(result)
    {
      var dNow = new Date();
      var localdate= (dNow.getMonth()+1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
      console.log(result.message+" Device is active @ "+localdate)
      deviceInfo()

    },
    error:function(err)
    {

     console.log("Device is inactive")


   }
 })
},600000)

// heart beat ends

}

$(document).ready(function () {




  $(".page").on('click', '.logo', function(event)
  {
    $(location).attr('href', 'login.html');
  });


  document.addEventListener("deviceready", onDeviceReady, false);

  function onDeviceReady() {
    /*alert("device ready");*/

    /*alert("before copy");*/

    window.plugins.sqlDB.copy("ashley.sqlite", copySuccess, copySuccess);
  }
  function copySuccess()
  {
    /*configure bg fetch*/
    bgfetch();
    /*configure bg fetch*/

    db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});

    db.transaction(function (tx) {

     var checkmarktng="select in_marketing from dealer_device_info where id=1"
     tx.executeSql(checkmarktng , [], function (tx, results) {
       var length=results.rows.length;
       var in_marketing=results.rows.item(0).in_marketing;
       console.log("in..marketing.."+in_marketing);
       if(in_marketing==1)
       {
         $(".pageHeader").append('<div class="homebutton headerButton" id="favourites" style="display:block;">My Favorites</div>');

       }
     });

   });

  }
  $(".page").on('click', '#favourites', function(event)
  {
    $(location).attr('href', 'favourites.html');
  });



  getWidthHeightRatio();
  setContentDimensions();
  scaleApp();
  showGridView();

     /* $('#productGridSlider .productGridImage').each(function () {
        $(this).css('background-image','url('+$(this).attr('data-image')+')');
      });*/






$('#searchInput').keypress(function(event){

//alert("search in search input");

var keycode = (event.keyCode ? event.keyCode : event.which);
if(keycode == '13'){
  var searchTag = $("#searchInput").val();
  if(searchTag == "" || searchTag == " " || searchTag == null )
  {
    $("#searchInput").val("");

    navigator.notification.alert(
              "Search input cannot be blank",    // message
              null,       // callback
              "Alert", // title
              'OK'        // buttonName
              );
  }
  else
  {
    $("#searchInput").val("");

    localStorage.setItem("search_tag",searchTag);
    $(location).attr("href","search.html")
  }
}
               // else{
                 //   alert("error");
                //}
                //Stop the event from propogation to other handlers
                //If this line will be removed, then keypress event handler attached
                //at document level will also be triggered
                event.stopPropagation();
              });


$("#search").click(function(){

 var searchTag = $("#searchInput").val();
 if(searchTag == "" || searchTag == " " || searchTag == null )
  {  $("#searchInput").val("");

navigator.notification.alert(
              "Search input cannot be blank",    // message
              null,       // callback
              "Alert", // title
              'OK'        // buttonName
              );
}
else
{
  $("#searchInput").val("");

  localStorage.setItem("search_tag",searchTag);

  $(location).attr("href","search.html")
}

})


$('.homebutton, .logo').on('tap', function () {
  showGridView();
});

    /*$('.box').on('tap', function () {
        showDetailView();
      });*/

$('.box1').on('tap', function () {
  showDetailView();
});

$('.catalog').on('click', function () {




toggleCatalog();

});

function checkmarketing()
{



}


$(".page").on('click', 'a.hasSub', function(event) 
{

  var parentid=$(this).closest("li").attr("id");
  var parentclass=$(this).closest("li").attr("class");
  /*alert("clicked"+parentid+"class.."+parentclass);*/
  /*var isVisible = $( '.maincatul #'+par ).is( ".opened" );*/
  if(parentclass=="maincatul")
  {
    var isopened=$(this).parent().find('ul').is( ".opened" );
    /*alert("is opened.."+isopened);*/
    if(isopened==false)
    {
      $('.categoryList ul ul').slideUp();

    }
    else if(isopened==true)
    {
     $('.categoryList > ul > li > a').removeClass('active');
   }
 }
 /*$('.categoryList ul ul').not("a.hasSub li#"+parentid+" ul li ul")slideUp();*/

 $(this).addClass('active').parent().find('ul').not("li#"+parentid+" ul li ul").slideToggle().toggleClass('opened');
});

$('.additionalImage').on('tap', function () {
  $('#detailMainImage').css('background-image','url(../images/dummy03.jpg)');
});

$('.searchButton').on('click', function () {
  toggleSearch();

});

$('.sortLink').on('tap', function () {
  $('.sortLink').removeClass('active');
  $(this).addClass('active');
});

$('#sortByStyle').on('tap', function () {
  $('#catalogCategories').addClass('styles');
});

$('#sortByRoom').on('tap', function () {
  $('#catalogCategories').removeClass('styles');
});

$('.categoryList a').on('tap', function () {
  $(this).closest('ul').find('a').removeClass('activeColor');
  $(this).addClass('activeColor');
});



$('.categoryList a').not('.hasSub').on('tap', function(){
  setTimeout(hideCatalog,200);
});

$('h2').on('tap', function () {
  if($(this).hasClass('active')){
            // do nothing
          } else {
            $('h2').removeClass('active').next().slideUp();
            $(this).addClass('active').next().slideDown();
          }
        }).next().hide();
$('h2:first').addClass('active').next().show();

/*
    $('#callMailPopup').on('tap', function () {

    });

    $('#callMobilePopup').on('tap', function () {

    });

    $('#callAssistancePopup').on('tap', function () {

    });
*/

var idleInterval = setInterval(timerIncrement, 5000);
$(window).on('tap touchstart touchend touchmove click', function () {
  setIdleToZero();
});
});

function deviceInfo()
{

// Device info

var infoKey = "api_key=OXe5fWizFQhu4acLd37llxqaYoEcryS1&device_id="+localStorage.getItem("device_id");

$.ajax({
  type: "POST", 
  url: "http://ashley.catalogkiosk.com/api/v1/get_device_info",
  contentType: "application/x-www-form-urlencoded",
  data :infoKey,
  dataType: "json",                   
  async: true, 
  success:function(result)
  {
    // localStorage.setItem("display_price",result.display_price);
    // localStorage.setItem("status",result.status);
    var logo_url=localStorage.getItem("dealer_logo_url");
    if(localStorage.getItem("logo")!=result.dealer_logo_url)
    {
      console.log("Logo changed...")
      downloadLogo(result.dealer_logo_url,"logo.jpg")
      localStorage.setItem("logo",result.dealer_logo_url);

    }
    // $(".logo").attr("src",logo_url);

  },
  error:function(error)
  {
    console.log(JSON.stringify(error))
  }
})


// device info ends

}


$(window).resize(function () {
  setContentDimensions();
  scaleApp();
  getWidthHeightRatio();
});

$(window).on("orientationchange", function (event) {
  setContentDimensions();
  scaleApp();
  getWidthHeightRatio();
});




function bgfetch()
{


  console.log('...............BackgroundFetch initiated........................................');

  var Fetcher = window.plugins.backgroundFetch;

    // Your background-fetch handler.
    var fetchCallback = function() {
      console.log('BackgroundFetch initiated');

        // perform your ajax request to server here
        $.ajax({url:"http://192.168.4.176:8080/ReadRFID/rest/service/get",success:function(result){


        }});
        
        syncallfetcher();
        Fetcher.finish();
        
      }

      Fetcher.configure(fetchCallback);

    }



    function syncallfetcher()
    {
      /* alert("sync called"); */
      db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});
      db.transaction(function (tx) {


       var checkdevice_id="select device_id from dealer_device_info where id=1"
       tx.executeSql(checkdevice_id , [], function (tx, results) {
         var length=results.rows.length;
         var device_id=results.rows.item(0).device_id;
         /*alert("device id.."+device_id); */
         console.log("in..device id ...."+device_id);
         if(device_id !="" && device_id!=null)
         {
           syncdatafetcher(device_id);
           console.log("..bg sync data called with device_id.."+device_id);

         }
       });

     });
    }


    function syncdatafetcher(device_id)
    {

      if(device_id!=null && device_id!="")
      {

        /*  alert(device_id); */
        
        console.log(".....sync data called...............");
        db = window.sqlitePlugin.openDatabase({name: "ashley.sqlite"});
        var apiKey="api_key=OXe5fWizFQhu4acLd37llxqaYoEcryS1&device_id="+device_id;
        /*alert("copy called");*/
        
        /*get products ajax starts*/
        $.ajax({
         type: "POST",
         url: "http://ashley.catalogkiosk.com/api/v1/get_products",
         contentType: "application/x-www-form-urlencoded",
         data :apiKey,
         dataType: "json",
         async: true,
         success:function(result)
         {
          /*alert("pro lngth.."+result[0].length);  */
          /*alert("successsss");*/

          $.each(result[0], function (index, value) {

            /*alert(result[0][index].name+"..length.."+result[0].length);*/
            var product_id=result[0][index].product_id;
            var brand_id=result[0][index].brand_id;
            var industry_id=result[0][index].industry_id;
            var name=result[0][index].name;
            var slug=result[0][index].slug;
            var description=result[0][index].description;
            var status=result[0][index].status;
            var price_wholesale=result[0][index].price_wholesale;
            var price_map=result[0][index].price_map;
            var sku=result[0][index].sku;
            var weight=result[0][index].weight;
            var volume=result[0][index].volume;
            var publish_visibility=result[0][index].publish_visibility;
            var publish_date=result[0][index].publish_date;
            var date_created=result[0][index].date_created;
            var timestamp=result[0][index].timestamp;
            var sync_at=result[0][index].sync_at;
            /*alert("sync at.."+sync_at+date_created);*/





            db.transaction(function (tx) {
             /*alert("inside trans..");*/
             var insertproduct="insert into products values ("+product_id+","+brand_id+","+industry_id+",'"+name+"','"+slug+"','"+description+"','"+status+"',"+price_wholesale+","+price_map+",'"+sku+"',"+weight+","+volume+",'"+publish_visibility+"','"+publish_date+"','"+date_created+"','"+timestamp+"','"+sync_at+"')";

             var updateproduct="UPDATE products SET brand_id="+brand_id+",industry_id="+industry_id+",name='"+name+"',slug='"+slug+"',description='"+description+"',status='"+status+"',price_wholesale="+price_wholesale+",price_map="+price_map+",sku='"+sku+"',weight="+weight+",volume="+volume+",publish_visibility='"+publish_visibility+"',publish_date='"+publish_date+"',date_created='"+date_created+"',timestamp='"+timestamp+"',sync_at='"+sync_at+"' WHERE product_id="+product_id+"";
             /*alert("update qry...."+updateproduct);*/
                               /*alert(insertproduct);
                               console.log(insertproduct);*/
                               
                               /*tx.executeSql(insertproduct);*/
                               
                               /*check whether the data is already available starts*/
                               var checkproduct="select name from products where product_id="+product_id+"";
                               tx.executeSql(checkproduct, [], function(tx, res) {
                                 var checklength=res.rows.length;
                                 /* alert("inside check product query"+checklength+" ..products available");*/

                                 if(checklength>0)
                                 {

                                   tx.executeSql(updateproduct, [], function(tx, res) {
                                     /* alert("insertId: " + res.insertId + " -- probably 1");  */
                                     /*alert(product_id+"..updated");*/
                                     console.log(product_id+"..product updated "+index);

                                   });
                                 }
                                 else
                                 {

                                   tx.executeSql(insertproduct, [], function(tx, res) {
                                     console.log("product table insertId: " + res.insertId + " "+index);

                                                           /* db.transaction(function(tx) {
                                                            tx.executeSql("select count(product_id) as cnt from products", [], function(tx, res) {
                                                            alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                                            alert("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
                                                            });
                                 }); */
                                 });
                                 }

                               });
/*check whether the data is already available ends*/



});

});/*loop ends(get products)*/





},
error: function (result) {
// alert("new pro err");
console.log(JSON.stringify(result))
}

});
/*1.get_products ajax ends*/




/*2.get_category ajax starts*/
$.ajax({
 type: "POST",
 url: "http://ashley.catalogkiosk.com/api/v1/get_category",
 contentType: "application/x-www-form-urlencoded",
 data :apiKey,
 dataType: "json",
 async: true,
 success:function(result)
 {

   console.log("get cat successsss");
   var iserror=result.error;
   console.log(iserror);
   if(iserror==false)
   {


     $.each(result[0], function (index, value) {
      /*alert(result[0][index].name+"..length.."+result[0].length);*/
      var category_id=result[0][index].category_id;
      var parent_category_id=result[0][index].parent_category_id;
      var name=result[0][index].name;
      var slug =result[0][index].slug;
      var sequence=result[0][index].sequence;
      var date_updated=result[0][index].date_updated;
      var sync_at=result[0][index].sync_at;
      /*alert(sync_at);*/

      db.transaction(function (tx) {
       /*alert("inside trans.."+db);*/

       var insertcategory="insert into category values ("+category_id+","+parent_category_id+",'"+name+"','"+slug+"',"+sequence+",'"+date_updated+"','"+sync_at+"')";

       var updatecategory="UPDATE category SET category_id="+category_id+",parent_category_id="+parent_category_id+",name='"+name+"',slug='"+slug+"',sequence="+sequence+",date_updated='"+date_updated+"',sync_at='"+sync_at+"' WHERE category_id="+category_id+"";
       /*alert("update qry...."+updatecategory);*/
       var checkproduct="select name from category where category_id="+category_id+"";
       tx.executeSql(checkproduct, [], function(tx, res) {
         var checklength=res.rows.length;
         /* alert("inside check category query .. "+checklength+" ..categories available");*/

         if(checklength>0)
         {

           tx.executeSql(updatecategory, [], function(tx, res) {
             /* alert("insertId: " + res.insertId + " -- probably 1");  */
             console.log(category_id+"..cat updated "+index);

           });
         }
         else
         {

          /*alert("inserting");*/
          tx.executeSql(insertcategory, [], function(tx, res) {
           console.log("cat table insertId: " + res.insertId +" "+ index);

                                                           /*  db.transaction(function(tx) {
                                                            tx.executeSql("select count(category_id) as cnt from category", [], function(tx, res) {
                                                            alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                                            
                                                            });
        }); */
        });
        }

      });


});
});




}/*if error false block ends*/


},
error: function (result) {
 console.log(JSON.stringify(result))
// alert("new cat err");
}

});
/*2.get_category ajax ends*/


/*3.product vs categories ajax starts*/
$.ajax({
 type: "POST",
 url: "http://ashley.catalogkiosk.com/api/v1/get_product_vs_categories",
 contentType: "application/x-www-form-urlencoded",
 data :apiKey,
 dataType: "json",
 async: true,
 success:function(result)
 {

   console.log("get pro vs cat successsss");
   var iserror=result.error;
   console.log(iserror);
   if(iserror==false)
   {
     /*alert(result[0]);*/
     
     $.each(result[0], function (index, value) {
      /*alert(result[0][index].name+"..length.."+result[0].length);*/

      var pvc_id=result[0][index].pvc_id;
      var product_id=result[0][index].product_id;
      var category_id=result[0][index].category_id;
      var sync_at=result[0][index].sync_at;
      /*alert(sync_at);*/

      db.transaction(function (tx) {
       /*alert("inside trans.."+db);*/

       var insertpro_vs_cat="insert into product_vs_categories values ("+pvc_id+","+product_id+","+category_id+",'"+sync_at+"')";

       var updatepro_vs_cat="UPDATE product_vs_categories SET pvc_id="+pvc_id+",product_id="+product_id+",category_id="+category_id+",sync_at='"+sync_at+"' WHERE pvc_id="+pvc_id+"";
       /*alert("update qry...."+updatecategory);*/
       var checkproduct="select product_id from product_vs_categories where pvc_id="+pvc_id+"";
       tx.executeSql(checkproduct, [], function(tx, res) {
         var checklength=res.rows.length;
         /* alert("inside check category query .. "+checklength+" ..categories available");*/

         if(checklength>0)
         {

           tx.executeSql(updatepro_vs_cat, [], function(tx, res) {
             /* alert("insertId: " + res.insertId + " -- probably 1");  */   
             console.log(pvc_id+"..pro vs cat updated "+index);

           }); 
         }
         else
         {

          /*alert("inserting");*/
          tx.executeSql(insertpro_vs_cat, [], function(tx, res) {
           console.log(" pro vs cat insertId: " + res.insertId+" "+index);     

                                                                       /*  db.transaction(function(tx) {
                                                                        tx.executeSql("select count(category_id) as cnt from category", [], function(tx, res) {
                                                                        alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                                                        
                                                                        });
        });*/
        });  
        }

      });


});
});




}/*if error false block ends*/


},
error: function (result) {
  console.log("new pro vs cat err");
}

});
/*3.product vs categories ajax ends*/


/*4.product vs images ajax ends*/

/*alert("pro vs img called");*/
console.log("pro vs img called");
$.ajax({
 type: "POST", 
 url: "http://ashley.catalogkiosk.com/api/v1/get_product_vs_images",
 contentType: "application/x-www-form-urlencoded",
 data :apiKey,
 dataType: "json",                   
 async: true, 
 success:function(result)
 {

   console.log("get pro vs images successsss");
   var iserror=result.error;
   console.log("pro vs img error.."+iserror);
   if(iserror==false)
   {
     /* console.log(JSON.stringify(result)); */
     /*alert(result[0]);*/


     $.each(result[0], function (index, value) {
      /*alert(result[0][index].name+"..length.."+result[0].length);*/
      var pvi_id=result[0][index].pvi_id;
      var product_id=result[0][index].product_id;
      var image_url=result[0][index].image_url;
      var local_image_name =result[0][index].local_image_name;
      var sync_at=result[0][index].sync_at;
      /* console.log(pvi_id+" "+product_id+" "+image_url+" "+local_image_name+" "+sync_at); */
      /*alert(sync_at);*/

      db.transaction(function (tx) {
       /*alert("inside trans.."+db);*/
       /*console.log("inside trans");*/
       var insertcategory="insert into product_vs_images values ("+pvi_id+","+product_id+",'"+image_url+"','"+local_image_name+"','"+sync_at+"')";
       /*console.log(insertcategory);*/
       var updatecategory="UPDATE product_vs_images SET pvi_id="+pvi_id+",product_id="+product_id+",image_url='"+image_url+"',local_image_name='"+local_image_name+"',sync_at='"+sync_at+"' WHERE pvi_id="+pvi_id+"";
       /*console.log(updatecategory);*/
       /*alert("update qry...."+updatecategory);*/
       var checkproduct="select product_id from product_vs_images where pvi_id="+pvi_id+"";
       tx.executeSql(checkproduct, [], function(tx, res) {
         var checklength=res.rows.length;
         /* alert("inside check category query .. "+checklength+" ..categories available");*/
         console.log(" length  "+checklength);

         if(checklength>0)
         {

          /* console.log("updating pro img table..");*/

          tx.executeSql(updatecategory, [], function(tx, res) {
           /* alert("insertId: " + res.insertId + " -- probably 1");  */   
           console.log(pvi_id+"..pro vs img updated "+index);

         }); 
        }
        else
        {

          /*console.log("inserting pro img table..");*/
          /*alert("inserting");*/
          tx.executeSql(insertcategory, [], function(tx, res) {
           console.log("pro vs img table insertId: " + res.insertId +" "+ index);    

                                                           /*  db.transaction(function(tx) {
                                                            tx.executeSql("select count(category_id) as cnt from category", [], function(tx, res) {
                                                            alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                                            
                                                            });
        }); */
        }); 
        }

      });


});
});




}/*if error false block ends*/


},
error: function (result) {
}

});
/*4.pro vs img ajax ends*/

}

}
